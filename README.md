# predicting_rpc

Files to reproduce the simulated data.
The workflow can be found in scripts/analyses.sh

Note that many of the script files (especially .jl files) point to a directory (variable name scriptfp) that needs to be changed to the location where all scripts will be stored.
