#!/bin/bash

#### Pascal Duenk, 2020-06-15 #######
#### pascal.duenk@wur.nl ############

#### this shell script shows how to run all analyses to get the results for the paper ####

#### PROGRAMS NEEDED ######

#### QMSIM 2.0 (20 Apr 2019)
#### R version 3.4.3 (2017-11-30)
#### Julia Version 0.7.0 (2018-08-08 06:46 UTC)
#### inquire about package versions

###########################################################
basefold="/" #### set the starting folder here
qtlfold="${basefold}/effects"

nqtl=1000

cd $basefolder
mkdir -p $qtlfold

##### sample the functional effects for each replicate ####
for rep in $(seq 1 20) 
do

  repl=$(printf "%03d" $rep) 

  ### sample additive ####
  julia scripts/sample_a.jl $repl $nqtl $qtlfold

  ### sample dominance effects ###
  julia scripts/sample_d.jl $repl none $qtlfold
  julia scripts/sample_d.jl $repl low $qtlfold
  
  ### sample epistatic coefficients ####
  julia scripts/sample_e.jl $repl none 5 $qtlfold
  julia scripts/sample_e.jl $repl low 5 $qtlfold

  ### sample 5 interactions per locus ####
  julia scripts/sample_epicombinations.jl $repl $nqtl 5 $qtlfold
  
done

#### FOR EVERY SCENARIO (GENETIC MODEL)
while read LINE
do

  domval=$(echo "$LINE" | cut -f1 -d' ')
  epival=$(echo "$LINE" | cut -f2 -d' ')
  epimod=$(echo "$LINE" | cut -f3 -d' ')
  
  ### remove and make folder, and copy parameter file ###
  nm="${domval}_${epival}_${epimod}"

  mkdir -p $nm
  cd $nm

  ### copy parameter file ###
  prm="${nm}.prm"	
  cp "${basefolder}/scripts/simulation2.prm" $prm
  cp "${basefolder}/masterseed" seed

  ### prepare shell scripts that compute EBV in QMSIM ####
  julia ../scripts/create_ebvsh.jl $domval $epival $epimod P50 51 20 0.3 $qtlfold NA
  julia ../scripts/create_ebvsh.jl $domval $epival $epimod P25 26 20 0.3 $qtlfold NA
  julia ../scripts/create_ebvsh.jl $domval $epival $epimod P10 11 20 0.3 $qtlfold NA

  julia ../scripts/create_ebvsh.jl $domval $epival $epimod N50 51 20 0.3 $qtlfold NA
  julia ../scripts/create_ebvsh.jl $domval $epival $epimod N25 26 20 0.3 $qtlfold NA
  julia ../scripts/create_ebvsh.jl $domval $epival $epimod N10 10 20 0.3 $qtlfold NA

  ### run simulation ######
  QMSim $prm -o 
  
  #### go to results folder ####
  cd $nm
	
	fold="r_${nm}"
	cd $fold 
  
  ##### COMPUTE RESULTS FOR EACH REPLICATE ###############
  for rep in $(seq 1 20) 
  do
    repl=$(printf "%03d" $rep) 
    
    #### format data to make it better readable for Julia ####
    data1="P50_data_${repl}.txt"
    data2="P25_data_${repl}.txt"
    data3="P10_data_${repl}.txt"
    data4="D_data_${repl}.txt"
    data5="N50_data_${repl}.txt"
    data6="N25_data_${repl}.txt"
    data7="N10_data_${repl}.txt"

    Rscript ../../scripts/format_data.R $data1 $data2 $data3 $data4 $data5 $data6 $data7
    
    ### check genetic trend ####
    julia ../../scripts/genetic_trend.jl $repl $domval $epival $epimod 1 50 8 P50 $qtlfold
    julia ../../scripts/genetic_trend.jl $repl $domval $epival $epimod 1 50 8 N50 $qtlfold
    
    ## compute allele frequencies in each line ###
    julia ../../scripts/ldphase.jl P25 P50 25 50 $repl nold
    julia ../../scripts/ldphase.jl P10 D 10 50 $repl nold
    julia ../../scripts/ldphase.jl N25 N50 25 50 $repl nold
    julia ../../scripts/ldphase.jl N10 D 10 50 $repl nold
    
    ### reduce genotype file size ####
    julia ../../scripts/reduce_genofile.jl $repl 50 P50
    julia ../../scripts/reduce_genofile.jl $repl 50 N50
    julia ../../scripts/reduce_genofile.jl $repl 50 D
    julia ../../scripts/reduce_genofile.jl $repl 25 P25
    julia ../../scripts/reduce_genofile.jl $repl 25 N25
    julia ../../scripts/reduce_genofile.jl $repl 10 N10
    julia ../../scripts/reduce_genofile.jl $repl 10 P10
    
    ### OPTIONAL: remove large genotype files ######
    #find . -name '*_mrk_qtl_${repl}.txt' -delete
    
    # average effects for PB performance in each line ####
    julia ../../scripts/average_effects.jl $repl 50 $domval $epival $epimod P50 variances.log $qtlfold
    julia ../../scripts/average_effects.jl $repl 25 $domval $epival $epimod P25 variances.log $qtlfold
    julia ../../scripts/average_effects.jl $repl 10 $domval $epival $epimod P10 variances.log $qtlfold
    julia ../../scripts/average_effects.jl $repl 50 $domval $epival $epimod D variances.log $qtlfold
    julia ../../scripts/average_effects.jl $repl 10 $domval $epival $epimod N10 variances.log $qtlfold
    julia ../../scripts/average_effects.jl $repl 25 $domval $epival $epimod N25 variances.log $qtlfold
    julia ../../scripts/average_effects.jl $repl 50 $domval $epival $epimod N50 variances.log $qtlfold
   
    #### compute average effects in the crossbreds, true genetic correlation and rpc 
    while read LINE
    do
      line1=$(echo "$LINE" | cut -f1 -d' ')
      line2=$(echo "$LINE" | cut -f2 -d' ')
      gen1=$(echo "$LINE" | cut -f3 -d' ')
      gen2=$(echo "$LINE" | cut -f4 -d' ')
      
      julia ../../scripts/average_effects_cb.jl $repl $gen1 $gen2 $domval $epival $epimod $line1 $line2 variances_cb.log $qtlfold 
      julia ../../scripts/truecorrelation.jl $repl $gen1 $gen2 $domval $epival $epimod $line1 $line2 rg.log
      
      ### compute the mean difference in allele frequencies
      julia ../../scripts/meandiffp.jl $repl 50 50 P50 N50 $domval $epival $epimod meandiffp.dat
      julia ../../scripts/meandiffp.jl $repl 25 25 P25 N25 $domval $epival $epimod meandiffp.dat
      julia ../../scripts/meandiffp.jl $repl 10 10 P10 N10 $domval $epival $epimod meandiffp.dat
      
    done < "${basefolder}/line_combs.dat"
    
  done
  
done < "${basefolder}/scenario_table.dat"

##### OTHER ANALYSES ####
### compute pedigree inbreeding 
Rscript scripts/ped_inbreeding.R

#### COLLECT RESULTS ######
cd $basefolder

bash scripts/collect_rg.sh
bash scripts/collect_meandiffp.sh

########  PLOT ALL THE RESULTS #####
######## NOTE THAT THESE SCRIPTS MAY NOT WORK ON ALL SYSTEMS DIRECTLY BECAUSE OF DIFFERENCES IN PACKAGE VERSIONS ETC. ###
######## BE SURE TO CHANGE SOME VARIABLES IN THE R FILES (FOLDERS ETC.) #####

cd $basefolder

### rg and variance plots ####
Rscript scripts/plot_rpc_predicted.R    #### Create Boxplot and scatterplots 
Rscript scripts/plot_meandiffp.R        #### check the difference in allele frequencies between parental lines for each scenario
