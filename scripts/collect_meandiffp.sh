#!/bin/bash
cd /lustre/nobackup/WUR/ABGC/duenk002/predict_rpc 

file1="../../../meandiffp.dat"

while read LINE
do 
	domval=$(echo "$LINE" | cut -f1 -d' ')
	epival=$(echo "$LINE" | cut -f2 -d' ')
	epimod=$(echo "$LINE" | cut -f3 -d' ')
	
	nm="${domval}_${epival}_${epimod}"
	
	cd $nm
	
	fold="r_${nm}"
	cd $fold 
	
	echo $nm 
	
	for r in $(seq 1 20)
	do
		repl=$(printf "%03d" $r)
		echo $repl 
		cd $repl
		cat meandiffp.dat >> $file1 
		cd .. 
	done 
	
	cd .. 
	cd ..
	
done < /lustre/nobackup/WUR/ABGC/duenk002/predict_rpc/scenario_table_2.dat 
