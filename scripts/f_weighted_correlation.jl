### calculate weighted correlation ######
function wcor(x,y,w)

  mx = sum(w .* x) / sum(w)
  my = sum(w .* y) / sum(w)

  sx = sum(w .* ((x .- mx).^2))/ sum(w)
  sy = sum(w .* ((y .- my).^2))/ sum(w)

  cv = sum(w .* (x .- mx) .* (y .- my)) / sum(w)

  wcorrelation = cv/(sqrt(sx)*sqrt(sy))
  
  return wcorrelation
end
