### create ebv.sh file for the simulation that is going to run ####
domval = ARGS[1]
epival = ARGS[2]
epimodel = ARGS[3]
breed = ARGS[4]
lastgen = ARGS[5]
lastrep = ARGS[6]
h2 = ARGS[7]
efolder = ARGS[8]
qtlfolder = ARGS[9]

fold = string(domval, "_", epival, "_", epimodel)

filenam = string("ebv_", breed, ".sh")

if (epival == "none") & (domval == "none") & (epimodel == "none")
	f=open(filenam, "w")
	write(f, "#!/bin/bash\nRscript /lustre/nobackup/WUR/ABGC/duenk002/armidale/scripts/format_data.R data.tmp ; \njulia /lustre/nobackup/WUR/ABGC/duenk002/armidale/scripts/ebv_none.jl $breed $fold $domval $epival $epimodel $lastgen $lastrep $h2 $efolder ;")
	close(f)
else
	f=open(filenam, "w")
	write(f, "#!/bin/bash\nRscript /lustre/nobackup/WUR/ABGC/duenk002/armidale/scripts/format_data.R data.tmp ; \njulia /lustre/nobackup/WUR/ABGC/duenk002/armidale/scripts/ebv.jl $breed $fold $domval $epival $epimodel $lastgen $lastrep $h2 $efolder $qtlfolder ;")
	close(f)
end

chmod(filenam, 0o777)