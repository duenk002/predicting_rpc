##### NOT REALLY USED ANYMORE. THIS FUNCTION IS NOW HARDCODED IN EBV.JL and TRUECORRELATION.JL #######
##### THE USE OF THIS WOULD ALSO BE INCORRECT, BECAUSE THIS FUNCTION ADDS ETOA to SIMBV, WHILE ALPHA WAS ALREADY UPDATED IN F_GETEFFECTS #####

#### function to get breeding values and total genetic values ###
### the effects in b are ordered as: mu, alpha1, d1, alpha2, aa, da, d2, ad, dd
###                                   1       2   3       4   5   6   7   8   9

function get_bvs(noia, noia_dom, epi_effects, epi_combinations, alpha, d)
	
	a1 = noia[:,epi_combinations[:,1]]
	a2 = noia[:,epi_combinations[:,2]]
	
	etoa1 = a1 * epi_effects[:,2]
	etoa2 = a2 * epi_effects[:,4]
	
	etoa = etoa1 .+ etoa2
	
	d1 = noia_dom[:,epi_combinations[:,1]]
	d2 = noia_dom[:,epi_combinations[:,2]]
	
	etod1 = d1 * epi_effects[:,3]
	etod2 = d2 * epi_effects[:,7]

	etod = etod1 .+ etod2

	simbv = (noia * alpha) .+ etoa
	simdom = (noia_dom * d) .+ etod

	####### get total genetic values #####################
	aa = a1 .* a2 * epi_effects[:,5]
	ad = a1 .* d2 * epi_effects[:,8]
	da = d1 .* a2 * epi_effects[:,6]
	dd = d1 .* d2 * epi_effects[:,9]

	simtgv = simbv .+ simdom .+ aa .+ ad .+ da .+ dd
	
	return simbv[:,1], simdom[:,1], simtgv[:,1], aa[:,1], ad[:,1], da[:,1], dd[:,1]
end
