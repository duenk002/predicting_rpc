########## Read simulated data from QMSim of 2 breeds
########## compute the allele frequencies 
########## Compute the correlation of LD phase using phased genotypes 

using CSV 
using Distributions
using DataFrames
using DelimitedFiles

if Sys.iswindows()
	const scriptfp = "C:/Projects/Armidale/HPC/scripts/"
else
	const scriptfp = "/lustre/nobackup/WUR/ABGC/duenk002/armidale/scripts/"
end

include("$scriptfp/f_readgeno.jl")
include("$scriptfp/f_allelefrequencies.jl")
include("$scriptfp/f_comp_ld_all.jl")

const breed1 = ARGS[1]
const breed2 = ARGS[2]
const gen1 = parse(Int16, ARGS[3])
const gen2 = parse(Int16, ARGS[4])
const repl = ARGS[5]
const ldoff = ARGS[6]  #### set to nold for allele frequencies only #####

const chrom = [1 3 5 8 10]

const datfile1 = String(breed1*"_data_"*repl*"_reformat.dat")
const datfile2 = String(breed2*"_data_"*repl*"_reformat.dat")
const gfile1 = String(breed1*"_mrk_qtl_"*repl*".txt")
const gfile2 = String(breed2*"_mrk_qtl_"*repl*".txt")
const mapfile = String("lm_mrk_qtl_"*repl*".txt")

if !isdir(repl)
	mkdir(repl)
end

### read linkage map ###
function readfile(fl)
	nline = countlines(fl)
	df = DataFrame(id = String[], chr = Int8[], pos = Float32[])
	
	f=open(fl)
	readline(f)
	for i in 1:(countlines(mapfile)-1)
		t = split(readline(f))
		a = t[[1]][1]
		b = parse(Int8, t[[2]][1])
		c = parse(Float32, t[[3]][1])
			
		push!(df, [a  b c])
	end
	close(f)
	return df
end

lmap = readfile(mapfile)

### read data file ###
dat1 = CSV.read(datfile1, delim=' ')
dat2 = CSV.read(datfile2, delim=' ')

### filter on generation ###
dat1 = dat1[dat1[:G] .== gen1,:]
dat2 = dat2[dat2[:G] .== gen2,:]

### selected ids ####
selids1 = convert(Array, dat1[:Progeny])
selids2 = convert(Array, dat2[:Progeny])

### read genotypes 
geno1, geno_phase1, nloc1 = read_genotypes(gfile1, selids1, true, true)
geno2, geno_phase2, nloc2 = read_genotypes(gfile2, selids2, true, true)

GC.gc()

### calculate allele frequencies ####
locinr = 1:nloc1
p1 = getaf(geno1)
p2 = getaf(geno2)

writedlm("$repl/allelefreq_"*breed1*"_$gen1.dat", p1, ' ')
writedlm("$repl/allelefreq_"*breed2*"_$gen2.dat", p2, ' ')

##### stop when LD computation is turned off ####
if ldoff == "nold"
	println("LD is not calculated, shutting down")
	exit()
end

## filter on allele frequency ####
# fix1 = [ (x .> 0.98) | (x .< 0.02) for x in p1]
# fix2 = [ (x .> 0.98) | (x .< 0.02) for x in p2]
# flt = (fix1 .| fix2)

# segregating = locinr[.!flt]

## select a random set of loci for LD calculation ###
# nseg = size(segregating)[1]
# sel = collect(1:3:nseg)
# selloci = segregating[sel]

selloci = convert(Array, readdlm("qtlloci_"*repl*".dat", Int64)[:,1])
    
#### filter the genotype file into qtl loci and marker loci
geno1 = geno1[:,selloci]
geno2 = geno2[:,selloci]

geno_phase1 = geno_phase1[:,selloci]
geno_phase2 = geno_phase2[:,selloci]

##### Filter the linkage map ####
lmap = lmap[selloci,:]

###
GC.gc()

## compute LD ###
ld1, ld2, ids, dist = compLD(geno1, geno2, lmap, p1[selloci], p2[selloci])

### write results ###
ld1dat = DataFrame(id1=ids[:,1], ids2=ids[:,2], dist=dist, ld=ld1)
ld2dat = DataFrame(id1=ids[:,1], ids2=ids[:,2], dist=dist, ld=ld2)

CSV.write("$repl/ld_"*breed1*"_$gen1"*"_$repl.dat", ld1dat, writeheader=false, delim=' ')
CSV.write("$repl/ld_"*breed2*"_$gen2"*"_$repl.dat", ld2dat, writeheader=false, delim=' ')

println("Done!")