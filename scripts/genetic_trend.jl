##### compute genetic values of all animals in all generations, using frequencies of generation 1 ####
using CSV 
using Distributions
using DataFrames
using DelimitedFiles
using LinearAlgebra
using Statistics

### set folder location of scripts ###
if Sys.iswindows()
	const scriptfp = "C:/Projects/Armidale/HPC/scripts/"
else
	const scriptfp = "/lustre/nobackup/WUR/ABGC/duenk002/armidale/scripts/"
end

include("$scriptfp/f_readgeno.jl")
include("$scriptfp/f_allelefrequencies.jl")
include("$scriptfp/f_noia_gfreq.jl")
include("$scriptfp/f_epistasis.jl")
include("$scriptfp/f_gfreq.jl")
include("$scriptfp/f_getbvs.jl")
include("$scriptfp/f_geteffects.jl")

###### argument variables ####
const repl = ARGS[1]
const domval = ARGS[2]
const epival = ARGS[3]
const epimodel = ARGS[4]
const mingen = parse(Int8, ARGS[5])
const maxgen = parse(Int8, ARGS[6])
const stepgen = parse(Int8, ARGS[7])
const breed = ARGS[8]
const efold = ARGS[9]

usegen = collect((mingen+stepgen):stepgen:(maxgen-stepgen))
usegen = [mingen; usegen; maxgen]
println(usegen)

#### other variables ####
const gfile = String(breed*"_mrk_qtl_"*repl*".txt")

#### read the ids with generations ####
dat = CSV.read(string(breed*"_data_",repl,"_reformat.dat"), header=true, delim=' ')
ids = convert(Array, dat[:Progeny])
gens = convert(Array, dat[:G])

flt = [in(x, usegen) for x in gens]

id_read = ids[ flt ]
gens = gens[ flt ]
genone = gens .== 1
nread = size(id_read)[1]

### read genotypes 
geno, geno_phase, nloc = read_genotypes(gfile, id_read, true, true)

### get effects ####
a = convert(Array, readdlm(efold*"/a_"*repl*".dat",' ', Float64))
d = convert(Array, readdlm(efold*"/d_"*domval*"_"*repl*".dat",' ', Float64))
epi_coef = convert(Array, readdlm(efold*"/e_"*epival*"_"*repl*".dat",' ', Float64))
### epi combinations ###
epi_combinations = convert(Array, readdlm(efold*"/epi_combinations_"*repl*".dat",' ', Int64))
nreqqtl = size(a)[1]

### get loci places ###
qtlloci = convert(Array, readdlm("qtlloci_"*repl*".dat", ' ', Int64)[:,1])

#### filter genotype matrix ####
geno = geno[:,qtlloci]
p = getaf(geno[genone,:])

### make NOIA matrix ######
gfreq = get_gfreq(geno[genone,:])
noia, noia_dom = make_noia(geno, gfreq)

### get epistatic effects ####
epi_vars, epi_effects = epi(gfreq, p, epi_coef, a, epimodel, epi_combinations)

##### get alphas to check #####
alphas, ds = get_effects(a,d,p,epi_combinations, epi_effects)

#### compute breeding values, dominance deviations etc. ####
println("Computing values")

simbv = noia * alphas
simdom = noia_dom * ds

if epimodel == "none"
	aa = zeros(Float32, nread)
	ad = zeros(Float32, nread)
	da = zeros(Float32, nread)
	dd = zeros(Float32, nread)
else
	a1 = noia[:,epi_combinations[:,1]]
	a2 = noia[:,epi_combinations[:,2]]

	d1 = noia_dom[:,epi_combinations[:,1]]
	d2 = noia_dom[:,epi_combinations[:,2]]

	aa = a1 .* a2 * epi_effects[:,5]
	ad = a1 .* d2 * epi_effects[:,8]
	da = d1 .* a2 * epi_effects[:,6]
	dd = d1 .* d2 * epi_effects[:,9]
end

simtgv = simbv .+ simdom .+ aa .+ ad .+ da .+ dd

#################################
### write results ###
if !isdir(repl)
	mkdir(repl)
end

dat2 = DataFrame(gen = gens, ids=id_read, bv=simbv, dom=simdom, aa=aa, ad=ad, da=da, dd=dd, tgv=simtgv)
CSV.write(string(repl,"/genetic_trend_",breed,".dat"), dat2, writeheader=true, delim=' ')

################################