#### compute alphas and all other effects following NOIA ####
function get_vars(alphas,ds,gfreq)
	println("Computing additive and dominance variances")
	nqtl = size(alphas)[1]
	Va = zeros(Float64, nqtl)
	Vd = zeros(Float64, nqtl)
	
	for i in 1:nqtl
		pAA = gfreq[i,1]
		pAa = gfreq[i,2]
		paa = gfreq[i,3]
		
		DA = [pAA 0 0; 0 pAa 0 ; 0 0 paa]

		sc_a = pAA+paa-((pAA-paa)^2)
		
		WA = zeros(Float64,3)
		WA[1] = -(-pAa - (2*paa))
		WA[2] = -(1-pAa - (2*paa))
		WA[3] = -(2-pAa - (2*paa))
		
		WD = zeros(Float64,3)
		if sc_a == 0.0   ### locus is fixed !!! ##
			WD .= 0.0
		else
			WD[1] = -2*pAa*paa/sc_a
			WD[2] = 4*pAA*paa/sc_a
			WD[3] = -2*pAA*pAa/sc_a
		end

		Va[i] = alphas[i] * (WA'*DA*WA) * alphas[i]	
		Vd[i] = ds[i] * (WD'*DA*WD) * ds[i]
	end
	
	return Va, Vd
end
