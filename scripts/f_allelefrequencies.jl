#### function to calculate allele frequencies ###############
function getaf(genmatrix, snpdim=2)
  af = zeros(Float64, size(genmatrix)[snpdim])
  for c in 1:size(genmatrix)[snpdim]
    if snpdim == 2
      af[c] = mean(genmatrix[:,c])/2
    else
      af[c] = mean(genmatrix[c,:])/2
    end
  end
  return af
end

### compute genotype frequencies ###
function gf(af1,af2)
	nqtl = size(af1)[1]
	gfreq = zeros(Float64, nqtl, 3)
	
	for i in 1:nqtl
		pAA = af1[i] * af2[i]
		pAa = (af1[i] * (1-af2[i])) + ((1-af1[i]) * af2[i])
		paa = (1-af1[i]) * (1-af2[i])
	
		gfreq[i,1] = pAA
		gfreq[i,2] = pAa
		gfreq[i,3] = paa
	end
	return gfreq
end

############################################################