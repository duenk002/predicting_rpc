########## compute the genetic correlation between traits in two breeds (A and B)
#################################################################################
using CSV 
using Distributions
using DataFrames
using DelimitedFiles
using LinearAlgebra

### set folder location of scripts ###
if Sys.iswindows()
	const scriptfp = "C:/Projects/Armidale/HPC/scripts/"
else
	const scriptfp = "/lustre/nobackup/WUR/ABGC/duenk002/armidale/scripts/"
end

include("$scriptfp/f_epistasis_cb.jl")
include("$scriptfp/f_geteffects.jl")
include("$scriptfp/f_getvariances.jl")
include("$scriptfp/f_allelefrequencies.jl")

###### argument variables ####
const repl = ARGS[1]
const gen1 = parse(Int16, ARGS[2])
const gen2 = parse(Int16, ARGS[3])
const domval = ARGS[4]
const epival = ARGS[5]
const epimodel = ARGS[6]
const breed1 = ARGS[7]
const breed2 = ARGS[8]
const outfile = ARGS[9]
const efolder = ARGS[10]

repchar = repl

#### qtl loci positions ####
qtlloci = convert(Array, readdlm("qtlloci_"*repchar*".dat", Int64)[:,1])
nqtl = size(qtlloci)[1]

### get effects ####
a = convert(Array, readdlm(efolder*"/a_"*repchar*".dat",' ', Float64))
d = convert(Array, readdlm(efolder*"/d_"*domval*"_"*repchar*".dat",' ', Float64))
epi_coef = convert(Array, readdlm(efolder*"/e_"*epival*"_"*repchar*".dat",' ', Float64))

### epi combinations ###
epi_combinations = convert(Array, readdlm(efolder*"/epi_combinations_"*repchar*".dat",' ', Int64))
epi_coef = epi_coef[1:size(epi_combinations)[1]]

##
nreqqtl = size(a)[1]

###### get allele frequencies ####
p1 = readdlm("$repchar/allelefreq_"*breed1*"_$gen1.dat", Float64)[:,1]
p2 = readdlm("$repchar/allelefreq_"*breed2*"_$gen2.dat", Float64)[:,1]
qtlp1 = p1[qtlloci]
qtlp2 = p2[qtlloci]

##### set fixed allele frequencies to an extreme frequency, so that the algorithm in epi() still works for all loci #####
qtlp1[ qtlp1 .== 1.0 ] .= 0.99999
qtlp1[ qtlp1 .== 0.0 ] .= 0.00001

qtlp2[ qtlp2 .== 1.0 ] .= 0.99999
qtlp2[ qtlp2 .== 0.0 ] .= 0.00001

###### compute (opposite) purebred and crossbred genotype frequencies ####
gfreq2 = gf(qtlp2, qtlp2)
gfreq3 = gf(qtlp1,qtlp2)

qtlp3 = (qtlp1 .+ qtlp2) ./ 2

###### compute effects ####
epi_vars1, epi_effects1 = epi_cb(gfreq3, gfreq2, qtlp3, epi_coef, a, epimodel, epi_combinations)
alphas1, ds1 = get_effects(a,d,qtlp2,epi_combinations, epi_effects1)

#### compute variances ####
Va1, Vd1 = get_vars(alphas1, ds1, gfreq3)

V_A1 = sum(Va1)
V_D1 = sum(Vd1)
V_I1 = sum(epi_vars1[:,[5 6 7]])

### write results ####
f=open(string(repchar,"/var_", breed1,"_", breed2,".log"), "a")
	write(f, "$domval $epival $epimodel $repl $breed1 $breed2 $V_A1 $V_D1 $V_I1 \n")
close(f)

### write alphas ####
writedlm(string(repchar, "/alphas_", breed1,"_", breed2,"_",repchar,".dat"), alphas1, ' ')

########################