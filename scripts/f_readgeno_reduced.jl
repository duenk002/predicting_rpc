function readgeno(fl , skiprow=false)
	
	f=open(fl)
	if skiprow == true
		readline(f)
	end
	
	firstl = split(readline(f))
	ncol = size(firstl)[1]
	nline = countlines(fl)
	
  geno = zeros(Int8, nline, ncol)
	
	f=open(fl)
	if skiprow == true
		readline(f)
	end
		
	for i in 1:(nline)
		geno[i,:] = [parse(Int8,x) for x in split(readline(f))]
	end
	close(f)
	
  return geno
  
end