using CSV 
using DataFrames
using DelimitedFiles

### set folder location of scripts ###
if Sys.iswindows()
	const scriptfp = "C:/Projects/Armidale/HPC/scripts/"
else
	const scriptfp = "/lustre/nobackup/WUR/ABGC/duenk002/armidale/scripts/"
end

include("$scriptfp/f_readgeno.jl")

###### argument variables ####
const repl = ARGS[1]
const gen1 = parse(Int16, ARGS[2])
const breed1 = ARGS[3]

#### other variables ####
const gfile1 = String(breed1*"_mrk_qtl_"*repl*".txt")
const datfile1 = String(breed1*"_data_"*repl*"_reformat.dat")

### read data file ###
dat1 = CSV.read(datfile1, delim=' ')

### filter on generation ###
dat1 = dat1[dat1[:G] .== gen1,:]

### selected ids ####
selids1 = convert(Array, dat1[:Progeny])

### read genotypes 
geno1, geno_phase1, nloc1 = read_genotypes(gfile1, selids1, true, true)

GC.gc()

### write genotypes
open(String(breed1*"_mrk_qtl_"*repl*"_red.txt"), "w") do io
  writedlm(io, geno1)
end
