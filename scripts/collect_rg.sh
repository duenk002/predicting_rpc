#!/bin/bash
cd /lustre/nobackup/WUR/ABGC/duenk002/predict_rpc 

file1="../../../rg.dat"
file2="../../../variances.dat"

while read LINE
do 
	domval=$(echo "$LINE" | cut -f1 -d' ')
	epival=$(echo "$LINE" | cut -f2 -d' ')
	epimod=$(echo "$LINE" | cut -f3 -d' ')
	
	nm="${domval}_${epival}_${epimod}"
	
	cd $nm
	
	fold="r_${nm}"
	cd $fold 
	
	echo $nm 
	
	for r in $(seq 1 20)
	do
		repl=$(printf "%03d" $r)
		echo $repl 
		cd $repl
		cat rg.log >> $file1 
		cat variances.log >> $file2 
		cd .. 
	done 
	
	cd .. 
	cd ..
	
done < /lustre/nobackup/WUR/ABGC/duenk002/predict_rpc/scenario_table.dat 
