########## compute the genetic correlation between traits in two breeds (A and B)
#################################################################################
using CSV 
using Distributions
using DataFrames
using DelimitedFiles
using LinearAlgebra

### set folder location of scripts ###
if Sys.iswindows()
	const scriptfp = "C:/Projects/Armidale/HPC/scripts/"
else
	const scriptfp = "/lustre/nobackup/WUR/ABGC/duenk002/armidale/scripts/"
end

include("$scriptfp/f_epistasis.jl")
include("$scriptfp/f_geteffects.jl")
include("$scriptfp/f_getvariances.jl")
include("$scriptfp/f_allelefrequencies.jl")

###### argument variables ####
const repl = ARGS[1]
const gen = parse(Int16, ARGS[2])
const domval = ARGS[3]
const epival = ARGS[4]
const epimodel = ARGS[5]
const breed = ARGS[6]
const outfile = ARGS[7]
const efolder = ARGS[8]

repchar = repl

#### qtl loci positions ####
qtlloci = convert(Array, readdlm("qtlloci_"*repchar*".dat", Int64)[:,1])
nqtl = size(qtlloci)[1]

### get effects ####
a = convert(Array, readdlm(efolder*"/a_"*repchar*".dat",' ', Float64))
d = convert(Array, readdlm(efolder*"/d_"*domval*"_"*repchar*".dat",' ', Float64))
epi_coef = convert(Array, readdlm(efolder*"/e_"*epival*"_"*repchar*".dat",' ', Float64))

### epi combinations ###
epi_combinations = convert(Array, readdlm(efolder*"/epi_combinations_"*repchar*".dat",' ', Int64))
epi_coef = epi_coef[1:size(epi_combinations)[1]]

##
nreqqtl = size(a)[1]

###### get allele frequencies ####
p1 = readdlm("$repchar/allelefreq_"*breed*"_$gen.dat", Float64)[:,1]
qtlp1 = p1[qtlloci]

##### set fixed allele frequencies to an extreme frequency, so that the algorithm in epi() still works for all loci #####
qtlp1[ qtlp1 .== 1.0 ] .= 0.99999
qtlp1[ qtlp1 .== 0.0 ] .= 0.00001

### compute genotype frequencies ######
gfreq1 = gf(qtlp1,qtlp1)

###### compute effects ####
epi_vars1, epi_effects1 = epi(gfreq1, qtlp1, epi_coef, a, epimodel, epi_combinations)
alphas1, ds1 = get_effects(a,d,qtlp1,epi_combinations, epi_effects1)

#### compute variances ####
Va1, Vd1 = get_vars(alphas1, ds1, gfreq1)
V_A1 = sum(Va1)
V_D1 = sum(Vd1)
V_I1 = sum(epi_vars1[:,[5 6 7]])

### write results ####
f=open("$repchar/$outfile","a")
	write(f, "$domval $epival $epimodel $gen $repl $breed $V_A1 $V_D1 $V_I1 \n")
close(f)

### write alphas ####
writedlm(string(repchar, "/alphas_", breed,"_",gen,"_",repchar,".dat"), alphas1, ' ')
########################