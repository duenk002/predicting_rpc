##### compute genetic values of all animals in all generations, using frequencies of generation 1 ####
using CSV 
using Distributions
using DataFrames
using DelimitedFiles
using LinearAlgebra
using Statistics

### set folder location of scripts ###
if Sys.iswindows()
	const scriptfp = "C:/Projects/Armidale/HPC/scripts/"
else
	const scriptfp = "/lustre/nobackup/WUR/ABGC/duenk002/armidale/scripts/"
end

#include("$scriptfp/f_readgeno.jl")
include("$scriptfp/f_allelefrequencies.jl")
include("$scriptfp/f_noia_gfreq.jl")
include("$scriptfp/f_gfreq.jl")
include("/lustre/nobackup/WUR/ABGC/duenk002/predict_rpc/scripts/f_weighted_correlation.jl")
include("/lustre/nobackup/WUR/ABGC/duenk002/predict_rpc/scripts/f_readgeno_reduced.jl")

###### argument variables ####
const repl = ARGS[1]
const gen1 = parse(Int16, ARGS[2])
const gen2 = parse(Int16, ARGS[3])
const domval = ARGS[4]
const epival = ARGS[5]
const epimodel = ARGS[6]
const breed1 = ARGS[7]
const breed2 = ARGS[8]
const outfile = ARGS[9]

#### other variables ####
const gfile1 = String(breed1*"_mrk_qtl_"*repl*"_red.txt")

### read genotypes 
geno1 = readgeno(gfile1)
nloc = size(geno1)[2]
GC.gc()

### get loci places ###
qtlloci = convert(Array, readdlm("qtlloci_"*repl*".dat", Int64)[:,1])

#### filter genotype matrix ####
geno1 = geno1[:,qtlloci]

###### get allele frequencies ####
p1 = readdlm("$repl/allelefreq_"*breed1*"_$gen1.dat", Float64)[:,1]
p2 = readdlm("$repl/allelefreq_"*breed2*"_$gen2.dat", Float64)[:,1]

qtlp1 = p1[qtlloci]
qtlp2 = p2[qtlloci]

### make NOIA matrix ######
gfreq1 = get_gfreq(geno1)
noia1, noia_dom1 = make_noia(geno1, gfreq1)

### get effects ####
alpha1 = convert(Array, readdlm(string(repl,"/alphas_",breed1,"_",gen1,"_",repl,".dat"), ' ', Float64))[:,1]
alpha2 = convert(Array, readdlm(string(repl,"/alphas_",breed2,"_",gen2,"_",repl,".dat"), ' ', Float64))[:,1]
alpha12 = convert(Array, readdlm(string(repl,"/alphas_",breed1,"_",breed2,"_", repl,".dat"), ' ', Float64))[:,1]

#### compute breeding values, dominance deviations etc. ####
println("Computing values")

simbvA1 = noia1 * alpha1
simbvA2 = noia1 * alpha2
simbvA12 = noia1 * alpha12

### correlation ####
rgbv = cor(simbvA1, simbvA2)
rpcbv = cor(simbvA1, simbvA12)

### covariances ####
cv = cov(simbvA1, simbvA2)
cvpc = cov(simbvA1, simbvA12)
v1 = var(simbvA1)
v2 = var(simbvA2)
v12 = var(simbvA12)

cva = cov(alpha1, alpha2)
cvpca = cov(alpha1, alpha12)
v1a = var(alpha1)
v2a = var(alpha2)
v12a = var(alpha12)

### weighted correlations based on alphas #####
w1 = 2 .* qtlp1 .* (1 .- qtlp1)
w2 = 2 .* qtlp2 .* (1 .- qtlp2)
w12 = (qtlp1 .* (1 .- qtlp2)) .+ ((1 .- qtlp1) .* qtlp2)

rg = cor(alpha1, alpha2)
rg1 = wcor(alpha1, alpha2, w1)
rg2 = wcor(alpha1, alpha2, w2)
rg12 = wcor(alpha1, alpha2, w12)

rpc = cor(alpha1, alpha12)
rpc1 = wcor(alpha1, alpha12, w1)
rpc2 = wcor(alpha1, alpha12, w2)
rpc12 = wcor(alpha1, alpha12, w12)

#################################
### write results ###
f=open("$repl/$outfile","a")
	write(f, "$domval $epival $epimodel $gen1 $gen2 $repl $breed1 $breed2 $rgbv $rpcbv $rg $rpc $cv $cvpc $v1 $v2 $v12 $cva $cvpca $v1a $v2a $v12a \n")
close(f)
################################
