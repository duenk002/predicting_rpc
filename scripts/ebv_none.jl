### compute ebv for selection within lines ###
###### THIS ONE SAMPLES THE QTL LOCI AND WRITES THEM TO FILES WITHIN THE RESULT FOLDER ####
###### THE LOCI FROM SCENARIO NONE_NONE_NONE ARE USED IN THE OTHER SELECTION SCENARIOS #####

using CSV
using DataFrames
using Distributions
using DelimitedFiles
using LinearAlgebra

## functions ###
if Sys.iswindows()
	const scriptfp = "C:/Projects/Armidale/HPC/scripts/"
else
	const scriptfp = "/lustre/nobackup/WUR/ABGC/duenk002/armidale/scripts/"
end

include("$scriptfp/f_readgeno.jl")
include("$scriptfp/f_allelefrequencies.jl")
include("$scriptfp/f_noia.jl")
#include("$scriptfp/f_epistasis_positive.jl")   ######### FOR POSITIVE EPISTASIS SCENARIOS!!! #@######
include("$scriptfp/f_epistasis.jl")  
include("$scriptfp/f_gfreq.jl")
include("$scriptfp/f_geteffects.jl")

#### parameters ####
const breed = ARGS[1]
const outfold = ARGS[2]
const domval = ARGS[3]
const epival = ARGS[4]
const epimodel = ARGS[5]
const lastgen = parse(Int64, ARGS[6])
const lastrep = parse(Int64, ARGS[7])
const h2 = parse(Float64, ARGS[8])
const efolder = ARGS[9]

### read temp data ###
dat1 = CSV.read("data_reformat.dat", delim=' ', header=true)
maxgen = maximum(dat1[:G])
id = convert(Array{Int64,1}, dat1[dat1[:G] .== maxgen, :Progeny])

println("Computing TBVs for generation $maxgen out of $lastgen")

### REPLICATE TRACKER: write a temp file to the directory to track what replicate we are working on #####
if isfile("rep.tmp")
	repl = readdlm("rep.tmp",' ', Int8)[1]
else
	repl = 1
end
repchar = lpad(string(repl),3,string(0))

f=open("rep.tmp", "w")
	write(f, repchar)
close(f)

### read genotypes ###
gfile = string("r_",outfold,"/",breed,"_mrk_qtl_",repchar*".txt")
geno, geno_phase, nloc = read_genotypes(gfile, id, true, true)

nread = size(geno)[1]
println("There were $nread genotypes read")

### get effects ####
a = convert(Array, readdlm(efolder*"/a_"*repchar*".dat",' ', Float64))
d = convert(Array, readdlm(efolder*"/d_"*domval*"_"*repchar*".dat",' ', Float64))
epi_coef = convert(Array, readdlm(efolder*"/e_"*epival*"_"*repchar*".dat",' ', Float64))

nreqqtl = size(a)[1]

### epi combinations ###
epi_combinations = convert(Array, readdlm(efolder*"/epi_combinations_"*repchar*".dat",' ', Int64))

##### if this is generation 0, then remove the old files ####
locinr = 1:(size(geno)[2])
if maxgen == 0
	rm("my_bv.txt", force=true)
	rm("all_bv.txt", force=true)
end

#### if the qtl are not sampled yet, sample them ####
if maxgen == 0
	qtlloci = sample(locinr, nreqqtl, replace=false)
	writedlm("r_"*outfold*"/qtlloci_"*repchar*".dat", qtlloci, ' ')
else
	qtlloci = convert(Array, readdlm("r_"*outfold*"/qtlloci_"*repchar*".dat", Int64)[:,1])
end

#### filter genotype matrix ####
geno = geno[:,qtlloci]
p = getaf(geno)

### compute genotype frequencies from random mating #####
gfreq = gf(p,p)

### get epistatic effects ####
epi_vars, epi_effects = epi(gfreq, p, epi_coef, a, epimodel, epi_combinations)

##### get alphas and ds #####
alphas, ds = get_effects(a,d,p,epi_combinations, epi_effects)

### compute noia matrices ####
noia, noia_dom = make_noia(geno)

#### breeding values and dominance deviations ####
simbv = noia * alphas
simdom = noia_dom * ds

if epimodel == "none"
	aa = zeros(Float32, nread)
	ad = zeros(Float32, nread)
	da = zeros(Float32, nread)
	dd = zeros(Float32, nread)
else
	a1 = noia[:,epi_combinations[:,1]]
	a2 = noia[:,epi_combinations[:,2]]

	d1 = noia_dom[:,epi_combinations[:,1]]
	d2 = noia_dom[:,epi_combinations[:,2]]

	aa = a1 .* a2 * epi_effects[:,5]
	ad = a1 .* d2 * epi_effects[:,8]
	da = d1 .* a2 * epi_effects[:,6]
	dd = d1 .* d2 * epi_effects[:,9]
end

simtgv = simbv .+ simdom .+ aa .+ ad .+ da .+ dd

#### phenotypes ####
vartgv = var(simtgv)
varresid = ((1-h2)/h2) * vartgv
resid = rand(Normal(0,sqrt(varresid)), nread)

pheno = simtgv .+ resid

### write results ###
dat = DataFrame(ids=id, ebvs=pheno)
dat2 = DataFrame(gen = maxgen, ids=id, bv=simbv, dom=simdom, aa=aa, ad=ad, da=da, dd=dd, tgv=simtgv, pheno=pheno)

### in generation >0, paste the breeding values with previous breeding values ###
if maxgen > 0
	#### for qmsim ###
	CSV.write("all_bv.txt", dat, writeheader=false, append=true, delim=' ')
	cp("all_bv.txt", "my_bv.txt")
	
	### to track true values ###
	CSV.write("tbvs.dat", dat2, writeheader=false, append=true, delim=' ')
else
	### for qmsim ###
	CSV.write("all_bv.txt", dat, writeheader=true, delim=' ')
	cp("all_bv.txt", "my_bv.txt")
	
	### to track true values ###
	CSV.write("tbvs.dat", dat2, writeheader=true, delim=' ')
end

##### If it is the last generation, replace the replicate number file ###
if maxgen == lastgen
	
	println("LAST GENERATION!")
	#### copy all the breeding values ###
	cp("all_bv.txt", String("r_"*outfold*"/my_pheno_"*repchar*".txt"))
	cp("tbvs.dat", String("r_"*outfold*"/my_tbvs_"*repchar*".txt"))
	
	### write the qtl effects ####
	writedlm("r_"*outfold*"/qtl_effects_"*repchar*".dat", [a d alphas ds], ' ')
	writedlm("r_"*outfold*"/epi_coef_"*repchar*".dat", epi_coef, ' ')
	
	### add 1 to the replicate ###
	repl += 1
	repchar2 = lpad(string(repl),3,string(0))
	
	#### change the replicate tracker ###
	if repl > lastrep
		rm("rep.tmp", force=true)
	else	
		f=open("rep.tmp", "w")
			write(f, repchar2)
		close(f)
	end
	
	println("End of replicate $repchar")
end

println("Finished!")